# Sample Project for DI on Spring

This is a sample project to show how we can inject multiple service implementations depending of a property or an argument.

## Add a new metric etl
  - Locate ETLSelectorType enum and add a new entry for the new metric.
  - For the new metric you need to define an extractor, processor and writer; so please verify if you can reuse any of those that are already coded.
  - In case you need to create a new extractor or processor or writer, please see the sections below; if not you're done 

## Create new extractor
  - Create new class called <something>ExtractorImpl under com.gorillalogic.service.extractor package
  - The new class <something>ExtractorImpl needs to implement ExtractorCommand
  - Add some code to the body of the 'execute' method
  - Then you need to subcribed the class as an available writer by adding a new entry in ExtractorCommand.Type enum pointing to <something>ExtractorImpl
  - Done, you have a new available extractor that can be used

## Create new processor
  - Create new class called <something>ProcessorImpl under com.gorillalogic.service.processor package
  - The new class <something>ProcessorImpl needs to implement ProcessorCommand
  - Add some code to the body of the 'execute' method
  - Then you need to subcribed the class as an available writer by adding a new entry in ProcessorCommand.Type enum pointing to <something>ProcessorImpl
  - Done, you have a new available processor that can be used

## Create new writer
  - Create new class called <something>WriterImpl under com.gorillalogic.service.writer package
  - The new class <something>WriterImpl needs to implement WriterCommand
  - Add some code to the body of the 'execute' method
  - Then you need to subcribed the class as an available writer by adding a new entry in WriterCommand.Type enum pointing to <something>WriterImpl
  - Done, you have a new available writer that can be used

Some notes:
  - We can reduce some classes by using one interface to containing the execute method for extractor, processors and writers; by doing that we are not protected to have only writer implementations in WriterType enum for example. 

## Tech

This is a sample project uses a number of open source projects to work properly:

* [spring-boot] - makes it easy to create stand-alone, production-grade Spring based Applications 
* [lombok] - reduce boiler-plate code in POJOs

License
----
MIT


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [spring-boot]: <https://projects.spring.io/spring-boot/>
   [lombok]: <https://projectlombok.org/>
