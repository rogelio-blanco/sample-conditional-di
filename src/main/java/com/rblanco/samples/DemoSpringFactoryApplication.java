package com.rblanco.samples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringFactoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringFactoryApplication.class, args);
	}
}
