package com.rblanco.samples.service.writer;

public interface WriterCommand<I, O> {
	public static enum Type {
		APPLE(AppleWriterImpl.class), ANDROID(AndroidWriterImpl.class);

		final Class<? extends WriterCommand<?, ?>> clazz;

		Type(Class<? extends WriterCommand<?, ?>> clazz) {
			this.clazz = clazz;
		}

		public Class<? extends WriterCommand<?, ?>> getClazz() {
			return clazz;
		}
	}

	O execute(I input);
}
