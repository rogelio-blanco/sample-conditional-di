package com.rblanco.samples.service.writer;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AppleWriterImpl implements WriterCommand<String, String> {

	@Override
	public String execute(String input) {
		log.info("Apple write");
		return input + " - Apple write";
	}

}
