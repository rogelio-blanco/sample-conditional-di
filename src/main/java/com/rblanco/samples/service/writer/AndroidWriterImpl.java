package com.rblanco.samples.service.writer;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AndroidWriterImpl implements WriterCommand<String, String> {

	@Override
	public String execute(String input) {
		log.info("Android write");
		return input + " - Android write";
	}

}
