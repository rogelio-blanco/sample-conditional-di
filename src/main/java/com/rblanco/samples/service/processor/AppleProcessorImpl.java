package com.rblanco.samples.service.processor;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AppleProcessorImpl implements ProcessorCommand<String, String> {

	@Override
	public String execute(String input) {
		log.info("Apple process");
		return input + " - Apple process";
	}

}
