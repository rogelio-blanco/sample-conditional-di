package com.rblanco.samples.service.processor;

public interface ProcessorCommand<I, O> {

	public static enum Type {
		APPLE(AppleProcessorImpl.class), ANDROID(AndroidProcessorImpl.class);

		final Class<? extends ProcessorCommand<?, ?>> clazz;

		Type(Class<? extends ProcessorCommand<?, ?>> clazz) {
			this.clazz = clazz;
		}

		public Class<? extends ProcessorCommand<?, ?>> getClazz() {
			return clazz;
		}
	}

	O execute(I i);

}
