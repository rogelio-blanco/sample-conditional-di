package com.rblanco.samples.service.processor;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AndroidProcessorImpl implements ProcessorCommand<String, String> {

	@Override
	public String execute(String input) {
		log.info("Android extract");
		return input + " - Android extract";
	}

}
