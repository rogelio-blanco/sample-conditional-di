package com.rblanco.samples.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.rblanco.samples.model.Source;
import com.rblanco.samples.service.extractor.ExtractorCommand;
import com.rblanco.samples.service.processor.ProcessorCommand;
import com.rblanco.samples.service.writer.WriterCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ETLExecutorService {
	@Autowired
	private ApplicationContext applicationContext;

	public void all(Source source) {
		if (source == null) {
			throw new IllegalArgumentException("Source cannot be null.");
		}

		if (source.getType() == null) {
			throw new IllegalArgumentException("Source type cannot be null.");
		}

		String wOutput = write(source.getType().getWriter(), process(source.getType().getProcessor(),
				extract(source.getType().getExtractor(), source.getIdentifier())));
		log.info(wOutput);
	}

	protected <I, O> O extract(ExtractorCommand.Type type, I input) {
		if (type == null || input == null) {
			throw new IllegalArgumentException("Argument type and input cannot be null.");
		}

		@SuppressWarnings("unchecked")
		ExtractorCommand<I, O> extractor = (ExtractorCommand<I, O>) applicationContext.getBean(type.getClazz());
		return extractor.execute(input);
	}

	protected <I, O> O process(ProcessorCommand.Type type, I input) {
		if (type == null) {
			throw new IllegalArgumentException("Argument type cannot be null.");
		}

		@SuppressWarnings("unchecked")
		ProcessorCommand<I, O> processor = (ProcessorCommand<I, O>) applicationContext.getBean(type.getClazz());
		return processor.execute(input);
	}

	protected <I, O> O write(WriterCommand.Type type, I input) {
		if (type == null) {
			throw new IllegalArgumentException("Argument type cannot be null.");
		}

		@SuppressWarnings("unchecked")
		WriterCommand<I, O> writer = (WriterCommand<I, O>) applicationContext.getBean(type.getClazz());
		return writer.execute(input);
	}
}
