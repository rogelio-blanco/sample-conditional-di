package com.rblanco.samples.service.extractor;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AppleExtractImpl implements ExtractorCommand<String, String> {

	@Override
	public String execute(String input) {
		log.info("Apple extract" + input);
		return "Apple extract";
	}

}
