package com.rblanco.samples.service.extractor;

public interface ExtractorCommand<I, O> {

	public static enum Type {
		APPLE(AppleExtractImpl.class), ANDROID(AndroidExtractImpl.class);

		final Class<? extends ExtractorCommand<?, ?>> clazz;

		Type(Class<? extends ExtractorCommand<?, ?>> clazz) {
			this.clazz = clazz;
		}

		public Class<? extends ExtractorCommand<?, ?>> getClazz() {
			return clazz;
		}
	}

	O execute(I input);
}
