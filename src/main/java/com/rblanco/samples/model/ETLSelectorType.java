package com.rblanco.samples.model;

import com.rblanco.samples.service.extractor.ExtractorCommand;
import com.rblanco.samples.service.processor.ProcessorCommand;
import com.rblanco.samples.service.writer.WriterCommand;

public enum ETLSelectorType {
	//// @formatter:off
	METRIC_DUMMY(ExtractorCommand.Type.ANDROID, ProcessorCommand.Type.ANDROID, WriterCommand.Type.APPLE), 
	METRIC_SOME(ExtractorCommand.Type.APPLE, ProcessorCommand.Type.APPLE, WriterCommand.Type.APPLE);
	// @formatter:on

	private final ExtractorCommand.Type extractor;
	private final ProcessorCommand.Type processor;
	private final WriterCommand.Type writer;

	ETLSelectorType(ExtractorCommand.Type extractor, ProcessorCommand.Type processor, WriterCommand.Type writer) {
		this.extractor = extractor;
		this.processor = processor;
		this.writer = writer;
	}

	public ExtractorCommand.Type getExtractor() {
		return extractor;
	}

	public ProcessorCommand.Type getProcessor() {
		return processor;
	}

	public WriterCommand.Type getWriter() {
		return writer;
	}
}
