package com.rblanco.samples.model;

import java.io.Serializable;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Source implements Serializable {
	private static final long serialVersionUID = -4965277409370350843L;

	private final String identifier;
	private final ETLSelectorType type;
}
