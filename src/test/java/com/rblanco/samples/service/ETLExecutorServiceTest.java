package com.rblanco.samples.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.rblanco.samples.model.ETLSelectorType;
import com.rblanco.samples.model.Source;
import com.rblanco.samples.service.ETLExecutorService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ETLExecutorServiceTest {

	@Autowired
	private ETLExecutorService etlExecutorService;

	@Test
	public void testExecuteAll() {
		etlExecutorService.all(new Source("com.project.id", ETLSelectorType.METRIC_DUMMY));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testExecuteAllWithNull() {
		etlExecutorService.all(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testExecuteAllWithSourceIdentifierNull() {
		etlExecutorService.all(new Source(null, ETLSelectorType.METRIC_DUMMY));
	}

	@Test(expected=IllegalArgumentException.class)
	public void testExecuteAllWithSourceETLSelectorTypeNull() {
		etlExecutorService.all(new Source("com.project.id", null));
	}

}
